package hr.ferit.bernardkekelic.tasky.recyclerview

object TaskRepository {

    val tasks: MutableList<Task>

    init {
        tasks = retrieveTasks()
    }

    private fun retrieveTasks(): MutableList<Task> {
        return mutableListOf()
    }
    fun remove(id: Int) = tasks.removeAll { task -> task.id == id }
    fun get(id: Int): Task? = tasks.find { task -> task.id == id }
    fun add(task: Task) = tasks.add(task)
    fun getMaxId(): Int{
        var maxId: Int = 0

        tasks.forEach {
            if(it.id > maxId)  maxId = it.id
        }
        return maxId
    }

}