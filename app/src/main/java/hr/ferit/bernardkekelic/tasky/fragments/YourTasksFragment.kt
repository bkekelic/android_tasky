package hr.ferit.bernardkekelic.tasky.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import hr.ferit.bernardkekelic.tasky.R
import hr.ferit.bernardkekelic.tasky.contexts.MyContext
import hr.ferit.bernardkekelic.tasky.database.TaskDatabase
import hr.ferit.bernardkekelic.tasky.recyclerview.TaskAdapter
import hr.ferit.bernardkekelic.tasky.recyclerview.TaskInteractionListener
import hr.ferit.bernardkekelic.tasky.recyclerview.TaskRepository
import kotlinx.android.synthetic.main.fragment_newtask.*
import kotlinx.android.synthetic.main.fragment_yourtasks.*

class YourTasksFragment: Fragment() {

    val tasksDao = TaskDatabase.getInstance().taskDao()

    companion object {
        fun newInstance(): YourTasksFragment{
            return YourTasksFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(R.layout.fragment_yourtasks, container, false)
        return view
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()

        displayData()
    }

    private fun initRecyclerView(){
        yourTasksDisplay.layoutManager = LinearLayoutManager(this.context, RecyclerView.VERTICAL, false)
        yourTasksDisplay.itemAnimator = DefaultItemAnimator()
        yourTasksDisplay.addItemDecoration(DividerItemDecoration(this.context, RecyclerView.VERTICAL))
    }

    private fun displayData(){

        val taskListener = object: TaskInteractionListener{
            override fun onRemove(id: Int) {
                TaskRepository.remove(id)
                (yourTasksDisplay.adapter as TaskAdapter).refreshData(TaskRepository.tasks)
            }

            override fun onShowDetails(id: Int) {
                val task = TaskRepository.get(id)
                Log.d("TAG", task.toString())
            }
        }

        getDataFromDb()
        yourTasksDisplay.adapter = TaskAdapter(TaskRepository.tasks, taskListener)
    }

    private fun getDataFromDb() {
        val yourDBTasks = tasksDao.getAll()

        yourDBTasks.forEach{
            TaskRepository.add(it)
        }
    }

}