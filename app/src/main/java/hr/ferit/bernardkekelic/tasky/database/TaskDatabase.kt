package hr.ferit.bernardkekelic.tasky.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import hr.ferit.bernardkekelic.tasky.contexts.MyContext
import hr.ferit.bernardkekelic.tasky.recyclerview.Task

@Database(version=1, entities = arrayOf(Task::class))
abstract class TaskDatabase: RoomDatabase() {

    abstract fun taskDao(): TaskDao

    companion object {
        private const val NAME = "yourTask database"
        private var INSTANCE: TaskDatabase? = null

        fun getInstance(): TaskDatabase{
            if(INSTANCE == null){

                INSTANCE = Room.databaseBuilder(
                    MyContext.ApplicationContext,
                    TaskDatabase::class.java,
                    NAME)
                    .allowMainThreadQueries()
                    .build()

            }
            return INSTANCE as TaskDatabase
        }
    }

}