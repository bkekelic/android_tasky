package hr.ferit.bernardkekelic.tasky.recyclerview

interface TaskInteractionListener {
    fun onRemove(id: Int)
    fun onShowDetails(id: Int)
}