package hr.ferit.bernardkekelic.tasky.fragments

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class FragmentAdapter(fragmentManager: FragmentManager): FragmentPagerAdapter(fragmentManager) {

    val fragments = arrayOf(
        YourTasksFragment.newInstance(),
        NewTaskFragment.newInstance()
    )
    val titles = arrayOf("YOUR TASKS", "NEW TASK")

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return titles[position]
    }

}