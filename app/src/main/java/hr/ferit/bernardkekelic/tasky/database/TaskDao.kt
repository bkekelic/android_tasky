package hr.ferit.bernardkekelic.tasky.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import hr.ferit.bernardkekelic.tasky.recyclerview.Task

@Dao
interface TaskDao {

    @Query("SELECT * FROM yourTasks")
    fun getAll(): List<Task>

    @Insert
    fun insert(task:Task)

    @Delete
    fun delete(task: Task)

}