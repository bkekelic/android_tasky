package hr.ferit.bernardkekelic.tasky.recyclerview

import android.app.AlertDialog
import android.support.v4.app.DialogFragment
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import hr.ferit.bernardkekelic.tasky.R
import hr.ferit.bernardkekelic.tasky.contexts.MyContext
import hr.ferit.bernardkekelic.tasky.database.TaskDatabase
import hr.ferit.bernardkekelic.tasky.ui.MainActivity
import kotlinx.android.synthetic.main.fragment_yourtasks.*
import kotlinx.android.synthetic.main.item_task.view.*

class TaskAdapter(
    tasks: MutableList<Task>,
    taskListener: TaskInteractionListener
): RecyclerView.Adapter<TaskHolder>() {

    private val tasks: MutableList<Task>
    private val taskListener: TaskInteractionListener

    companion object {
        lateinit var recyclerViewInstance: RecyclerView
    }

    init {
        this.tasks = mutableListOf()
        this.tasks.addAll(tasks)
        this.taskListener = taskListener
    }

    fun refreshData(tasks: MutableList<Task>){
        this.tasks.clear()
        this.tasks.addAll(tasks)
        this.notifyDataSetChanged()
    }
    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)

        recyclerViewInstance = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskHolder {
        val taskView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_task, parent, false)
        val taskHolder = TaskHolder(taskView)
        return taskHolder
    }

    override fun getItemCount(): Int {
        return tasks.size
    }

    override fun onBindViewHolder(holder: TaskHolder, position: Int) {
        val task = tasks[position]
        holder.bind(task, taskListener)
    }
}

class TaskHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    fun bind(task: Task, taskListener: TaskInteractionListener){

        itemView.itemTaskTitle.text = task.title
        itemView.itemTaskCategory.text = task.category
        if(task.priority == "Low priority") itemView.itemTaskPriorityImage.setImageResource(R.drawable.green)
        else if(task.priority == "Medium priority") itemView.itemTaskPriorityImage.setImageResource(R.drawable.yellow)
        else if(task.priority == "High priority") itemView.itemTaskPriorityImage.setImageResource(R.drawable.red)

        itemView.setOnClickListener{ taskListener.onShowDetails(task.id) }
        itemView.setOnLongClickListener { removeTask(task, taskListener); true; }

    }

    private fun removeTask(task: Task, taskListener: TaskInteractionListener){
        val tasksDao = TaskDatabase.getInstance().taskDao()
        taskListener.onRemove(task.id)
        tasksDao.delete(task)
        Toast.makeText(MyContext.ApplicationContext, "Task deleted", Toast.LENGTH_SHORT).show()
    }
}
