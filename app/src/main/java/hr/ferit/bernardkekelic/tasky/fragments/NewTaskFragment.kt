package hr.ferit.bernardkekelic.tasky.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import hr.ferit.bernardkekelic.tasky.R
import hr.ferit.bernardkekelic.tasky.contexts.MyContext
import hr.ferit.bernardkekelic.tasky.database.TaskDatabase
import hr.ferit.bernardkekelic.tasky.recyclerview.Task
import hr.ferit.bernardkekelic.tasky.recyclerview.TaskAdapter
import hr.ferit.bernardkekelic.tasky.recyclerview.TaskInteractionListener
import hr.ferit.bernardkekelic.tasky.recyclerview.TaskRepository
import hr.ferit.bernardkekelic.tasky.ui.MainActivity
import kotlinx.android.synthetic.main.fragment_newtask.*
import kotlinx.android.synthetic.main.fragment_yourtasks.*

class NewTaskFragment: Fragment(), AdapterView.OnItemSelectedListener{


    private var SPINNER_CATEGORY_ID: String = ""
    private var SPINNER_PRIORITY_ID: String = ""

    private var selectedCategory: String = ""
    private var selectedPriority: String = ""

    val tasksDao = TaskDatabase.getInstance().taskDao()

    companion object {
        fun newInstance(): NewTaskFragment{
            return NewTaskFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_newtask, container, false)
        return view
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        getSpinnersFromId()
        fillSpinners(view)

        NewTaskAction.setOnClickListener { saveNewTask() }
    }
    override fun onNothingSelected(parent: AdapterView<*>?) {}
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

        if(parent?.id.toString() == SPINNER_CATEGORY_ID){
            selectedCategory = parent?.getItemAtPosition(position).toString()
        }
        else if(parent?.id.toString() == SPINNER_PRIORITY_ID){
            selectedPriority = parent?.getItemAtPosition(position).toString()
        }
    }

    private fun getSpinnersFromId() {
        SPINNER_CATEGORY_ID = NewTask_CategorySpinner.id.toString()
        SPINNER_PRIORITY_ID = NewTask_PrioritySpinner.id.toString()
    }
    private fun fillSpinners(view: View) {
        val categorySpinner: Spinner = view.findViewById(R.id.NewTask_CategorySpinner)
        val prioritySpinner: Spinner = view.findViewById(R.id.NewTask_PrioritySpinner)

        categorySpinner.onItemSelectedListener = this
        prioritySpinner.onItemSelectedListener = this

        // Create category spinner
        ArrayAdapter.createFromResource(
            MyContext.ApplicationContext,
            R.array.spinnerCategory_array,
            R.layout.spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(R.layout.spinner_dropdown_item)
            categorySpinner.adapter = adapter
        }

        // Create priority spinner
        ArrayAdapter.createFromResource(
            MyContext.ApplicationContext,
            R.array.spinnerPriority_array,
            R.layout.spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(R.layout.spinner_dropdown_item)
            prioritySpinner.adapter = adapter
        }


    }

    private fun saveNewTask() {

        if(newTask_Title.text.isNotEmpty()){
            val title = newTask_Title.text.toString()
            val length: Int = TaskRepository.getMaxId()
            val newTask = Task(id = length+1, title = title, category = selectedCategory,priority =  selectedPriority)
            newTask_Title.setText("")

            tasksDao.insert(newTask)
            TaskRepository.add(newTask)
            (TaskAdapter.recyclerViewInstance.adapter as TaskAdapter).refreshData(TaskRepository.tasks)

            Toast.makeText(MyContext.ApplicationContext, "Task saved", Toast.LENGTH_SHORT).show()
        }else{
            Toast.makeText(MyContext.ApplicationContext, "Please input task title!", Toast.LENGTH_SHORT).show()
        }


    }

}