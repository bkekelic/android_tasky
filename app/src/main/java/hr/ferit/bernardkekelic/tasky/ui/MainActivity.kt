package hr.ferit.bernardkekelic.tasky.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import hr.ferit.bernardkekelic.tasky.R
import hr.ferit.bernardkekelic.tasky.contexts.MyContext
import hr.ferit.bernardkekelic.tasky.fragments.FragmentAdapter
import hr.ferit.bernardkekelic.tasky.recyclerview.TaskAdapter
import hr.ferit.bernardkekelic.tasky.recyclerview.TaskRepository
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_newtask.*
import kotlinx.android.synthetic.main.fragment_yourtasks.*

class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setUpFragments()
    }

    private fun setUpFragments() {
        viewPager.adapter = FragmentAdapter(supportFragmentManager)
        tabLayout.setupWithViewPager(viewPager)
    }






}
